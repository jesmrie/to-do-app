import os
import urllib

from google.appengine.ext import ndb

import jinja2
import webapp2
import logging
import json

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

DEFAULT_TODO_NAME = 'default_todo'

def todo_key(todo_name = DEFAULT_TODO_NAME):
    return ndb.Key('ToDo', DEFAULT_TODO_NAME)

class ToDo(ndb.Model):
    content = ndb.StringProperty()
    status = ndb.IntegerProperty()
    date = ndb.DateTimeProperty(auto_now_add=True)

# [START main_page]
class MainPage(webapp2.RequestHandler):
    def get(self):
        # todo_name = self.request.get('todo_name',
        #                                   DEFAULT_TODO_NAME)
        # todo_query = ToDo.query(
        #     ancestor=todo_key(todo_name)).order(-ToDo.content)
        # todo = todo_query.fetch()

        # template_values = {
        #     'todo': todo
        # }
        # logging.critical(template_values)

        template = JINJA_ENVIRONMENT.get_template('build/index.html')
        self.response.write(template.render())
# [END main_page]

class GetToDos(webapp2.RequestHandler):
    def get(self):
        todos = ToDo.query().order(ToDo.date)
        todo_json   = []

        for todo in todos:
            logging.critical(todo)
            todo_dict = {
                'content' : todo.content,
                'status' : todo.status,
            }
            todo_json.append(todo_dict)
        
        
        self.response.write(json.dumps(todo_json))

class ToDoHandler(webapp2.RequestHandler):
    def post(self):
        content = self.request.get("content")
        todo = ToDo(content=content, status=0)
        todo.put()
        # logging.critical("ToDoHandler")

        self.redirect('/')
    
    def delete(self):
        return

# [START app]
app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/todo', ToDoHandler),
    ('/getToDos', GetToDos),
], debug=True)
# [END app]

