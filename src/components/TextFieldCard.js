import React, { Component } from 'react'

class TextFieldCard extends Component {
    state = {
        content: ''
    }

    handleChange = (e) => {
        this.setState({
            content: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.addTodo(this.state);
        this.setState({
            content: ''     
        })
        const data = new FormData(e.target);
        fetch('/todo', {
            method: 'post',
            body: data
        }).then(res => {
            return res;
        }).catch(err => err);
    }

    render(){
        return (
            <div>
                <form onSubmit = {this.handleSubmit}>
                    <input type="text" name="content" onChange={this.handleChange} placeholder="Add To Do" value={this.state.content} />
                </form>
            </div>
        )
    }
}

export default TextFieldCard