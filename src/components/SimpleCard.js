import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';
import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';

const useStyles = makeStyles({
  card: {
    width: "75%",
    textAlign: 'left',
    margin: 5,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  content: {
    marginTop: 12,
    fontSize: 14,
  },
  icon: {
    fontSize: 32,
    color: '#584C61'
  },
});

const SimpleCard = ({todos, deleteTodo}) => {
  const classes = useStyles();
  const todoList = todos.length ? (
      todos.map(todo => {
          return (
            <Card className={classes.card} key = {todo.id}>
              <CardContent>
                <Grid container spacing={1}>
                  <Grid item xs={2} md={1}>
                    <Checkbox />
                  </Grid>
                  <Grid item xs={8} md={10}>
                    <Typography variant="h5" component="h2" className={classes.content}>
                      {todo.content}
                    </Typography>
                  </Grid>
                  <Grid item xs={2} md={1}>
                    <button onclick={() => {deleteTodo(todo.content)}}><DeleteOutlinedIcon className={classes.icon} /></button>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          )
      })
  ) : (
      <p className="notice">You have no todo's left, yay!</p>
  )
  return (
      <div className="container">
          {todoList}
      </div>
  )

}

export default SimpleCard;