import React, { Component } from 'react';
import 'typeface-roboto';
import '@material-ui/icons';
import './App.css';
import Typography from '@material-ui/core/Typography';
import SimpleCard from './components/SimpleCard';
import TextFieldCard from './components/TextFieldCard';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }
  // state = {
  //   todos: [
  //     {id: 1, content: 'Buy groceries'},
  //     {id: 2, content: 'Birthday gift for John'}
  //   ]
  // }

  componentDidMount() {
    fetch("/getToDos")
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          this.setState({
            isLoaded: true,
            items: result
          });
          
          console.log(this.state);
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  addTodo = (item) => {
    item.id = Math.random();
    let items = [...this.state.items, item];
    this.setState({
      items
    })
  }

  deleteTodo = (content) => {
    console.log(content);
    const items = this.state.items.filter(item => {
      return item.content !== content
    });
    this.setState({
      items
    })
  }

  render() {
    return (
      <div className="App">
          <Typography variant="h2" component="h2" className="date">
            <b>1 June</b>
          </Typography>
          <Typography variant="h6" component="h2" className="header">
            <b>It'll be a quite busy day. Stay efficient!</b>
          </Typography>
          <SimpleCard todos = {this.state.items} deleteTodo={this.deleteTodo} />
          <TextFieldCard addTodo = {this.addTodo} />
      </div>
    );
  }
}

export default App;
